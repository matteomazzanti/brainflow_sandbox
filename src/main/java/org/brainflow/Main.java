package org.brainflow;

import java.util.Arrays;

import brainflow.BoardShim;
import brainflow.BrainFlowInputParams;

public class Main {

    public static void mainx(String[] args) throws Exception
    {
        BoardShim.enable_board_logger ();
        BrainFlowInputParams params = new BrainFlowInputParams ();
        int board_id = parse_args (args, params);
        BoardShim board_shim = new BoardShim (board_id, params);

        int sampling_rate = BoardShim.get_sampling_rate (board_id);
        System.out.println("Sampling Rate: " + sampling_rate);

        board_shim.prepare_session ();
        board_shim.start_stream (); // use this for default options

        // int u = 0;
//        while(true) {
//            //System.out.println(u);
//            double[][] data = board_shim.get_board_data();
//            for (int i = 0; i < data.length; i++) {
//                if (data[i].length != 0) {
//                    System.out.println(Arrays.toString(data[i]));
//                }
//            }
//        }
    }

    public static void main(String[] args) throws Exception
    {
        BoardShim.enable_board_logger ();
        BrainFlowInputParams params = new BrainFlowInputParams ();
        int board_id = parse_args (args, params);
        BoardShim board_shim = new BoardShim (board_id, params);

        int sampling_rate = BoardShim.get_sampling_rate (board_id);
        System.out.println("Sampling Rate: " + sampling_rate);
        int package_num_channel = BoardShim.get_package_num_channel (board_id);
        System.out.println("package_num_channel: " + package_num_channel);
        int  get_num_rows = BoardShim. get_num_rows (board_id);
        System.out.println("get_num_rows: " +  get_num_rows);
        String[] get_eeg_names = BoardShim.get_eeg_names (board_id);
        System.out.println("get_eeg_names: ");
        for (int i = 0; i < get_eeg_names.length; i++) {
            System.out.println(get_eeg_names[i]);
        }
        board_shim.prepare_session ();
        board_shim.start_stream (250); // use this for default options

//        while(true) {
//            double[][] data = board_shim.get_board_data(10);
//            for (int i = 0; i < data.length; i++) {
//                if (data[i].length != 0) {
//                    System.out.println(Arrays.toString(data[i]));
//                }
//            }
//        }
    }

    public static void main4(String[] args) throws Exception
    {
        BoardShim.enable_board_logger ();
        BrainFlowInputParams params = new BrainFlowInputParams ();
        int board_id = parse_args (args, params);
        BoardShim board_shim = new BoardShim (board_id, params);

        int sampling_rate = BoardShim.get_sampling_rate (board_id);
        System.out.println("Sampling Rate: " + sampling_rate);

        board_shim.prepare_session ();
        board_shim.start_stream (); // use this for default options

        // int u = 0;
        while(true) {
            //System.out.println(u);
            double[][] data = board_shim.get_board_data();
            for (int i = 0; i < data.length; i++) {
                if (data[i].length != 0) {
                    System.out.println(Arrays.toString(data[i]));
                }
            }
        }
    }

    public static void main3(String[] args) throws Exception
    {
        BoardShim.enable_board_logger ();
        BrainFlowInputParams params = new BrainFlowInputParams ();
        int board_id = parse_args (args, params);
        BoardShim board_shim = new BoardShim (board_id, params);

        board_shim.prepare_session ();
        board_shim.start_stream (); // use this for default options

        // int u = 0;
        for (int u = 0; u < 10000; u++) {
            //System.out.println(u);
            double[][] data = board_shim.get_board_data();
            for (int i = 0; i < data.length; i++) {
                if (data[i].length != 0) {
                    System.out.println(Arrays.toString(data[i]));
                }
            }
        }
    }


    public static void main2(String[] args) throws Exception
    {
        BoardShim.enable_board_logger ();
        BrainFlowInputParams params = new BrainFlowInputParams ();
        int board_id = parse_args (args, params);
        BoardShim board_shim = new BoardShim (board_id, params);

        board_shim.prepare_session ();
        board_shim.start_stream (); // use this for default options
        // board_shim.start_stream (450000, "file://file_stream.csv:w");
        // BoardShim.log_message (LogLevels.LEVEL_INFO.get_code (), "Start sleeping in the main thread");
        // Thread.sleep (5000);
        // board_shim.stop_stream ();
        System.out.println (board_shim.get_board_data_count ());
        // double[][] data = board_shim.get_current_board_data (30); // doesn't flush it from ring buffer
        double[][] data = board_shim.get_board_data (); // get all data and flush
        // from ring buffer
        for (int i = 0; i < data.length; i++)
        {
            System.out.println (i);
            System.out.println (Arrays.toString (data[i]));
        }
        // board_shim.release_session ();
    }



    private static int parse_args (String[] args, BrainFlowInputParams params)
    {
        int board_id = -1;
        for (int i = 0; i < args.length; i++)
        {
            if (args[i].equals ("--ip-address"))
            {
                params.ip_address = args[i + 1];
            }
            if (args[i].equals ("--serial-port"))
            {
                params.serial_port = args[i + 1];
            }
            if (args[i].equals ("--ip-port"))
            {
                params.ip_port = Integer.parseInt (args[i + 1]);
            }
            if (args[i].equals ("--ip-protocol"))
            {
                params.ip_protocol = Integer.parseInt (args[i + 1]);
            }
            if (args[i].equals ("--other-info"))
            {
                params.other_info = args[i + 1];
            }
            if (args[i].equals ("--board-id"))
            {
                board_id = Integer.parseInt (args[i + 1]);
            }
            if (args[i].equals ("--timeout"))
            {
                params.timeout = Integer.parseInt (args[i + 1]);
            }
            if (args[i].equals ("--serial-number"))
            {
                params.serial_number = args[i + 1];
            }
            if (args[i].equals ("--file"))
            {
                params.file = args[i + 1];
            }
        }
        return board_id;
    }

}
